package facci.pm.alexaherrera.retrofitpost;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.ButtonBarLayout;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import facci.pm.alexaherrera.retrofitpost.adapter.MarketAdapter;
import facci.pm.alexaherrera.retrofitpost.model.Post;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    EditText editText1, editText2,editText3;
    Button button1, button2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        InitView();
        button1.setOnClickListener(this);
        button2.setOnClickListener(this);

//       PostPost();
    }

    private void InitView() {
        editText1 = findViewById(R.id.editText);
        editText2 = findViewById(R.id.editText2);
        editText3 = findViewById(R.id.editText3);
        button1 = findViewById(R.id.button);
        button2 = findViewById(R.id.button2);
    }

    private void PostPost(){
        MarketAdapter adapter = new MarketAdapter();
        Call<Post> call = adapter.InsertPost(
                new Post(
                        editText1.getText().toString(),
                        editText2.getText().toString(),
                        "https://static.misionesonline.news/wp-content/uploads/2016/08/CUENCPLADI2-7ggrqre9o5n0.bmp"
                ));
        call.enqueue(new Callback<Post>() {
            @Override
            public void onResponse(Call<Post> call, Response<Post> response) {
                Log.e("response", response.body().toString());
                //GetPosts();
            }

            @Override
            public void onFailure(Call<Post> call, Throwable t) {

            }
        });




    }


    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.button:
                PostPost();
                break;

            case R.id.button2:
                Intent intent = new Intent( MainActivity.this, Main2Activity.class);
                startActivity(intent);
                break;
        }
    }
}
